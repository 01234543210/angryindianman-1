import pygame
pygame.init()

window = pygame.display.set_mode((750, 1000))
pygame.display.set_caption('Project')
walkRight = [pygame.image.load('right-2.png'),
             pygame.image.load('right-3.png')]
walkLeft = [pygame.image.load('left-2.png'),
            pygame.image.load('left-3.png')]
walkUp = [pygame.image.load('up-2.png'),
          pygame.image.load('up-3.png')]
walkDown = [pygame.image.load('down-2.png'),
            pygame.image.load('down-3.png')]
stand = pygame.image.load('up-1.png')
stand2 = pygame.image.load('down-1.png')
bg = pygame.image.load('Bg.png')
GameOver = pygame.image.load('GameOverScreen.png')
gameover = False
Victory = pygame.image.load('victory.png')
stationary = pygame.image.load('Stationary.png')
clock = pygame.time.Clock()
score1 = 0
score2 = 0
state1 = 0
state2 = 0
 
class enemy(object):

    moveRight = [pygame.image.load('car-right.png'),
                 pygame.image.load('car-right - 2.png'),
                 pygame.image.load('car-right - 3.png'),
                 pygame.image.load('car-right - 4.png')]
    moveLeft = [pygame.image.load('car-left.png'),
                pygame.image.load('car-left - 2.png'),
                pygame.image.load('car-left - 3.png'),
                pygame.image.load('car-left - 4.png')]

    def __init__(self,x,y,width,height,end):
        self.x = x
        self.y = y
        self.width = width
        self.height = height
        self.end = end
        self.path = [self.x, self.end]
        self.walkcount = 0
        self.vel = 5
        self.vel2 = 5
        self.status = 0

    def draw(self, window):

        self.move()

        if self.walkcount + 10 >= 40:
            self.walkcount = 0

        if self.vel > 0:
            window.blit(self.moveRight[self.walkcount // 10], (self.x,self.y))
            self.walkcount += 10
        else:
            window.blit(self.moveLeft[self.walkcount // 10], (self.x,self.y))
            self.walkcount += 10

    def move(self):
        if self.vel > 0:
            if self.x + self.vel < self.path[1]:
                self.x += self.vel
            else:
                self.vel = self.vel * -1
                self.walkcount = 0
        else:
            if self.x - self.vel > self.path[0]:
                self.x += self.vel
            else:
                self.vel = self.vel * -1
                self.walkcount = 0
                
class enemy2(object):
        def __init__(self, x, y , width, height):
                self.x = x
                self.y = y
                self.width = width
                self.height = height
                self.status = 0
        def draw(self, window):
                window.blit(stationary, (self.x, self.y))

        
stationary1= enemy2(60, 235, 75, 30)
stationary2 = enemy2(160, 405, 75, 30)
stationary3 = enemy2(125, 575, 75, 30)
stationary4 = enemy2(570, 750, 75, 30)
memecar = enemy(20, 150, 75, 30, 650)
memecar2 = enemy(20, 320, 75, 30, 650)
memecar3 = enemy(20, 490, 75, 30, 650)
memecar4 = enemy(20, 665, 75, 30, 650)
memecar5 = enemy(20, 840, 75, 30, 650)
memecar.vel = 15
memecar2.vel = 20
memecar3.vel = 25
memecar4.vel = 20
memecar5.vel = 15


class player(object):

    def __init__(self,x,y,width,height):
        self.x = x
        self.y = y
        self.width = width
        self.height = height
        self.vel = 8
        self.left = False
        self.right = False
        self.up = False
        self.down = False
        self.walkcount = 0
        self.score = 0
        self.status = 0
        
        
        
    def draw(self, window):
        if self.walkcount + 10 >= 60:
            self.walkcount = 0
        if self.left:
            window.blit(walkLeft[self.walkcount // 30], (self.x,self.y))
            self.walkcount += 10
        elif self.right:
            window.blit(walkRight[self.walkcount // 30], (self.x,self.y))
            self.walkcount += 10
        elif self.up:
            window.blit(walkUp[self.walkcount // 30], (self.x, self.y))
            self.walkcount += 10
        elif self.down:
            window.blit(walkDown[self.walkcount // 30], (self.x,self.y))
            self.walkcount += 10
        else:
            window.blit(stand, (self.x, self.y))

class player2(object):

    def __init__(self,x,y,width,height):
        self.x = x
        self.y = y
        self.width = width
        self.height = height
        self.vel = 8
        self.left = False
        self.right = False
        self.up = False
        self.down = False
        self.walkcount = 0
        self.score = 0
        self.status = 0

    def draw(self, window):
        if self.walkcount + 10 >= 60:
            self.walkcount = 0
        if self.left:
            window.blit(walkLeft[self.walkcount // 30], (self.x,self.y))
            self.walkcount += 10
        elif self.right:
            window.blit(walkRight[self.walkcount // 30], (self.x,self.y))
            self.walkcount += 10
        elif self.up:
            window.blit(walkUp[self.walkcount // 30], (self.x, self.y))
            self.walkcount += 10
        elif self.down:
            window.blit(walkDown[self.walkcount // 30], (self.x,self.y))
            self.walkcount += 10
        else:
            window.blit(stand2, (self.x, self.y))


angryIndianman = player(360, 950, 75, 30)
angryIndianman2 = player2(360, 30, 75, 30)


def redrawGameWindow1():
    text1 = pygame.font.Font('FreeSansBold.ttf', 22)
    score1 = text1.render('yo', True, (255,255,255))
    window.blit(score1, (120,30))
    window.blit(bg, (0, 0))
    angryIndianman.draw(window)
    window.blit(stand2 , (360, 30))
    stationary1.draw(window)
    stationary2.draw(window)
    stationary3.draw(window)
    stationary4.draw(window)
    memecar.draw(window)
    memecar2.draw(window)
    memecar3.draw(window)
    memecar4.draw(window)
    memecar5.draw(window)
    pygame.display.update()

def redrawGameWindow2():
    window.blit(bg, (0, 0))
    angryIndianman2.draw(window)
    window.blit(stand, (360, 950))
    stationary1.draw(window)
    stationary2.draw(window)
    stationary3.draw(window)
    stationary4.draw(window)
    memecar.draw(window)
    memecar2.draw(window)
    memecar3.draw(window)
    memecar4.draw(window)
    memecar5.draw(window)
    pygame.display.update()

def endscreen():
    not_escape = True
    print(angryIndianman.score)
    print(angryIndianman2.score)
    if angryIndianman.score > angryIndianman2.score:
       print('Player 1 has won')
       winplayer="Player1 has won"
    else:
       winplayer="Player2 has won"
       
    while not_escape:
         window.fill((0,0,0))
         font=pygame.font.Font('freesansbold.ttf',50)
         winner=font.render(winplayer,True,(255,255,255))
         window.blit(winner,(200,450))
         player1score=font.render("Player1 Score "+str(angryIndianman.score),True,(255,255,255))
         player2score=font.render("Player2 Score "+str(angryIndianman2.score),True,(255,255,255))
         window.blit(player1score,(200,550))
         window.blit(player2score,(200,650))
         keys1 = pygame.key.get_pressed()
         for event in pygame.event.get():
             if event.type == pygame.QUIT:
                not_escape = False
             if keys1[pygame.K_SPACE]:
                angryIndianman.x = 360
                angryIndianman.y = 950
                angryIndianman2.x = 360
                angryIndianman2.y = 120
                game1()
         pygame.display.flip()
    pygame.quit()
    quit()


# My game loop
def game2():
        memecar.status = 0
        memecar2.status= 0
        memecar3.status = 0
        memecar4.status = 0
        memecar5.status = 0
        stationary1.status = 0
        stationary2.status = 0
        stationary3.status = 0
        stationary4. status = 0
        Min = 0
        angryIndianman2.score = 0
        angryIndianman.x = 360
        angryIndianman.y = 950
        run = True
        scoreIncrease,t,trail = pygame.USEREVENT+1,1000,[]
        pygame.time.set_timer(scoreIncrease,t)
        while run:
            clock.tick(60)
            redrawGameWindow2()
            keys = pygame.key.get_pressed()
            for event in pygame.event.get():
                if event.type == pygame.QUIT:
                        pygame.quit()
                if event.type == scoreIncrease:
                        angryIndianman2.score += 1
            print(angryIndianman2.score)
            if keys[pygame.K_LEFT] and angryIndianman2.x > angryIndianman2.vel:
                        angryIndianman2.x -= angryIndianman2.vel
                        angryIndianman2.left = True
                        angryIndianman2.right = False
                        angryIndianman2.up = False
                        angryIndianman2.down = False
                        gameover = False
            elif keys[pygame.K_RIGHT] and angryIndianman2.x < 705 - angryIndianman2.vel:
                        angryIndianman2.x += angryIndianman2.vel
                        angryIndianman2.right = True
                        angryIndianman2.left = False
                        angryIndianman2.up = False
                        angryIndianman2.down = False
                        gameover = False
            elif keys[pygame.K_UP] and angryIndianman2.y > angryIndianman2.vel:
                        angryIndianman2.y -= angryIndianman2.vel
                        angryIndianman2.right = False
                        angryIndianman2.left = False
                        angryIndianman2.up = True
                        angryIndianman2.down = False
                        gameover = False
            elif keys[pygame.K_DOWN] and angryIndianman2.y < 980 - angryIndianman2.vel:
                        angryIndianman2.y += angryIndianman2.vel
                        angryIndianman2.right = False
                        angryIndianman2.left = False
                        angryIndianman2.down = True
                        angryIndianman2.up = False
                        gameover = False
            else:
                        angryIndianman2.right = False
                        angryIndianman2.left = False
                        angryIndianman2.up = False
                        angryIndianman2.down = False
                        angryIndianman2.walkcount = 0
                        gameover = False
            if (angryIndianman2.y > Min):
                        Min = angryIndianman2.y
                        if (Min > stationary1.y and stationary1.status == 0):
                                  angryIndianman2.score += 5
                                  stationary1.status = 1
                        if (Min > stationary2.y and stationary2.status == 0):
                                  angryIndianman2.score += 5
                                  stationary2.status =1
                        if (Min > stationary3.y and stationary3.status == 0):
                                  angryIndianman2.score += 5
                                  stationary3.status =1
                        if (Min > stationary4.y and stationary4.status == 0):
                                  angryIndianman2.score += 5
                                  stationary4.status =1          
                        if (Min > memecar.y and memecar.status == 0):
                                  angryIndianman2.score += 10
                                  memecar.status = 1
                        if (Min > memecar2.y and memecar2.status == 0):
                                  angryIndianman2.score += 10
                                  memecar2.status = 1
                        if (Min > memecar3.y and memecar3.status == 0):
                                  angryIndianman2.score += 10
                                  memecar3.status = 1
                        if (Min > memecar4.y and memecar4.status == 0):
                                  angryIndianman2.score += 10
                                  memecar4.status = 1
                        if (Min > memecar5.y and memecar5.status == 0):
                                  angryIndianman2.score += 10
                                  memecar5.status = 1         
            if (angryIndianman2.x + 55 > memecar.x and angryIndianman2.x+ 55 < memecar.x + 150 or angryIndianman2.x > memecar.x and angryIndianman2.x < memecar.x + 150) and (angryIndianman2.y > memecar.y and angryIndianman2.y< memecar.y + 60 or angryIndianman2.y + 60> memecar.y and angryIndianman2.y < memecar.y):
                        window.blit(stand, (360,950)) 
                        endscreen()
            if (angryIndianman2.x + 55 > memecar2.x and angryIndianman2.x+ 55 < memecar2.x + 150 or angryIndianman2.x > memecar2.x and angryIndianman2.x < memecar2.x + 150) and (angryIndianman2.y > memecar2.y and angryIndianman2.y< memecar2.y + 60 or angryIndianman2.y + 60> memecar2.y and angryIndianman2.y < memecar2.y):
                        window.blit(stand, (360,950))
                        endscreen()
            if (angryIndianman2.x + 55 > memecar3.x and angryIndianman2.x+ 55 < memecar3.x + 150 or angryIndianman2.x > memecar3.x and angryIndianman2.x < memecar3.x + 150) and (angryIndianman2.y > memecar3.y and angryIndianman2.y< memecar3.y + 60 or angryIndianman2.y + 60> memecar3.y and angryIndianman2.y < memecar3.y):
                        window.blit(stand, (360,950))
                        endscreen()
            if (angryIndianman2.x + 55 > memecar4.x and angryIndianman2.x+ 55 < memecar4.x + 150 or angryIndianman2.x > memecar4.x and angryIndianman2.x < memecar4.x + 150) and (angryIndianman2.y > memecar4.y and angryIndianman2.y< memecar4.y + 60 or angryIndianman2.y + 60> memecar4.y and angryIndianman2.y < memecar4.y):
                        window.blit(stand, (360,950))
                        endscreen()
            if (angryIndianman2.x + 55 > memecar5.x and angryIndianman2.x+ 55 < memecar5.x + 150 or angryIndianman2.x > memecar5.x and angryIndianman2.x < memecar5.x + 150) and (angryIndianman2.y > memecar5.y and angryIndianman2.y< memecar5.y + 60 or angryIndianman2.y + 60> memecar5.y and angryIndianman2.y < memecar5.y):
                        window.blit(stand, (360,950))
                        endscreen()
            if (angryIndianman2.x + 55 > stationary1.x and angryIndianman2.x + 55 < stationary1.x + 150 or angryIndianman2.x > stationary1.x and angryIndianman2.x < stationary1.x + 150) and (angryIndianman2.y > stationary1.y and angryIndianman2.y < stationary1.y + 60 or angryIndianman2.y + 60 > stationary1.y and angryIndianman2.y < stationary1.y):
                        window.blit(stand, (360, 950))
                        endscreen()
            if (angryIndianman2.x + 55 > stationary2.x and angryIndianman2.x + 55 < stationary2.x + 150 or angryIndianman2.x > stationary2.x and angryIndianman2.x < stationary2.x + 150) and (angryIndianman2.y > stationary2.y and angryIndianman2.y < stationary2.y + 60 or angryIndianman2.y + 60 > stationary2.y and angryIndianman2.y < stationary2.y):
                        window.blit(stand, (360, 950))
                        endscreen()
            if (angryIndianman2.x + 55 > stationary3.x and angryIndianman2.x + 55 < stationary3.x + 150 or angryIndianman2.x > stationary3.x and angryIndianman2.x < stationary3.x + 150) and (angryIndianman2.y > stationary3.y and angryIndianman2.y < stationary3.y + 60 or angryIndianman2.y + 60 > stationary3.y and angryIndianman2.y < stationary3.y):
                        window.blit(stand, (360, 950))
                        endscreen()
            if (angryIndianman2.x + 55 > stationary4.x and angryIndianman2.x + 55 < stationary4.x + 150 or angryIndianman2.x > stationary4.x and angryIndianman2.x < stationary4.x + 150) and (angryIndianman2.y > stationary4.y and angryIndianman2.y < stationary4.y + 60 or angryIndianman2.y + 60 > stationary4.y and angryIndianman2.y < stationary4.y):
                        window.blit(stand, (360, 950))
                        endscreen()           
            if angryIndianman2.y > 950:
                        angryIndianman2.status = 1
                        pygame.display.update()
                        endscreen()
def game1():
        memecar.status = 0
        memecar2.status= 0
        memecar3.status = 0
        memecar4.status = 0
        memecar5.status = 0
        stationary1.status = 0
        stationary2.status = 0
        stationary3.status = 0
        stationary4. status = 0
        run = True
        Max = 1000
        angryIndianman.score = 0
        angryIndianman2.x = 360
        angryIndianman2.y = 30
        scoreIncrease,t,trail = pygame.USEREVENT+1,1000,[]
        pygame.time.set_timer(scoreIncrease,t)   
        while run:
            print(angryIndianman.score)
            clock.tick(60)
            redrawGameWindow1()
            keys = pygame.key.get_pressed()
            for event in pygame.event.get():
                if event.type == pygame.QUIT:
                        pygame.quit()
                if event.type == scoreIncrease:
                        angryIndianman.score += 1
            if keys[pygame.K_LEFT] and angryIndianman.x > angryIndianman.vel:
                        angryIndianman.x -= angryIndianman.vel
                        angryIndianman.left = True
                        angryIndianman.right = False
                        angryIndianman.up = False
                        angryIndianman.down = False
                        gameover = False
            elif keys[pygame.K_RIGHT] and angryIndianman.x < 705 - angryIndianman.vel:
                        angryIndianman.x += angryIndianman.vel
                        angryIndianman.right = True
                        angryIndianman.left = False
                        angryIndianman.up = False
                        angryIndianman.down = False
                        gameover = False
            elif keys[pygame.K_UP] and angryIndianman.y > angryIndianman.vel:
                        angryIndianman.y -= angryIndianman.vel
                        angryIndianman.right = False
                        angryIndianman.left = False
                        angryIndianman.up = True
                        angryIndianman.down = False
                        gameover = False
            elif keys[pygame.K_DOWN] and angryIndianman.y < 980 - angryIndianman.vel:
                        angryIndianman.y += angryIndianman.vel
                        angryIndianman.right = False
                        angryIndianman.left = False
                        angryIndianman.down = True
                        angryIndianman.up = False
                        gameover = False
            else:
                        angryIndianman.right = False
                        angryIndianman.left = False
                        angryIndianman.up = False
                        angryIndianman.down = False
                        angryIndianman.walkcount = 0
                        gameover = False
            if (angryIndianman.y < Max):
                        Max = angryIndianman.y
                        if (Max < stationary1.y and stationary1.status == 0):
                                  angryIndianman.score += 5
                                  stationary1.status = 1
                        if (Max < stationary2.y and stationary2.status == 0):
                                  angryIndianman.score += 5
                                  stationary2.status =1
                        if (Max < stationary3.y and stationary3.status == 0):
                                  angryIndianman.score += 5
                                  stationary3.status =1
                        if (Max < stationary4.y and stationary4.status == 0):
                                  angryIndianman.score += 5
                                  stationary4.status =1          
                        if (Max < memecar.y and memecar.status == 0):
                                  angryIndianman.score += 10
                                  memecar.status = 1
                        if (Max < memecar2.y and memecar2.status == 0):
                                  angryIndianman.score += 10
                                  memecar2.status = 1
                        if (Max < memecar3.y and memecar3.status == 0):
                                  angryIndianman.score += 10
                                  memecar3.status = 1
                        if (Max < memecar4.y and memecar4.status == 0):
                                  angryIndianman.score += 10
                                  memecar4.status = 1
                        if (Max < memecar5.y and memecar5.status == 0):
                                  angryIndianman.score += 10
                                  memecar5.status = 1         
            if (angryIndianman.x + 55 > memecar.x and angryIndianman.x+ 55 < memecar.x + 150 or angryIndianman.x > memecar.x and angryIndianman.x < memecar.x + 150) and (angryIndianman.y > memecar.y and angryIndianman.y< memecar.y + 60 or angryIndianman.y + 60> memecar.y and angryIndianman.y < memecar.y):
                        window.blit(stand, (360,950))
                        game2()
            if (angryIndianman.x + 55 > memecar2.x and angryIndianman.x+ 55 < memecar2.x + 150 or angryIndianman.x > memecar2.x and angryIndianman.x < memecar2.x + 150) and (angryIndianman.y > memecar2.y and angryIndianman.y< memecar2.y + 60 or angryIndianman.y + 60> memecar2.y and angryIndianman.y < memecar2.y):
                        window.blit(stand, (360,950))
                        game2()
            if (angryIndianman.x + 55 > memecar3.x and angryIndianman.x+ 55 < memecar3.x + 150 or angryIndianman.x > memecar3.x and angryIndianman.x < memecar3.x + 150) and (angryIndianman.y > memecar3.y and angryIndianman.y< memecar3.y + 60 or angryIndianman.y + 60> memecar3.y and angryIndianman.y < memecar3.y):
                        window.blit(stand, (360,950))
                        game2()
            if (angryIndianman.x + 55 > memecar4.x and angryIndianman.x+ 55 < memecar4.x + 150 or angryIndianman.x > memecar4.x and angryIndianman.x < memecar4.x + 150) and (angryIndianman.y > memecar4.y and angryIndianman.y< memecar4.y + 60 or angryIndianman.y + 60> memecar4.y and angryIndianman.y < memecar4.y):
                        window.blit(stand, (360,950))
                        game2()
            if (angryIndianman.x + 55 > memecar5.x and angryIndianman.x+ 55 < memecar5.x + 150 or angryIndianman.x > memecar5.x and angryIndianman.x < memecar5.x + 150) and (angryIndianman.y > memecar5.y and angryIndianman.y< memecar5.y + 60 or angryIndianman.y + 60> memecar5.y and angryIndianman.y < memecar5.y):
                        window.blit(stand, (360,950))
                        game2()
            if (angryIndianman.x + 55 > stationary1.x and angryIndianman.x + 55 < stationary1.x + 150 or angryIndianman.x > stationary1.x and angryIndianman.x < stationary1.x + 150) and (angryIndianman.y > stationary1.y and angryIndianman.y < stationary1.y + 60 or angryIndianman.y + 60 > stationary1.y and angryIndianman.y < stationary1.y):
                        window.blit(stand, (360, 950))
                        game2()
            if (angryIndianman.x + 55 > stationary2.x and angryIndianman.x + 55 < stationary2.x + 150 or angryIndianman.x > stationary2.x and angryIndianman.x < stationary2.x + 150) and (angryIndianman.y > stationary2.y and angryIndianman.y < stationary2.y + 60 or angryIndianman.y + 60 > stationary2.y and angryIndianman.y < stationary2.y):
                        window.blit(stand, (360, 950))
                        game2()
            if (angryIndianman.x + 55 > stationary3.x and angryIndianman.x + 55 < stationary3.x + 150 or angryIndianman.x > stationary3.x and angryIndianman.x < stationary3.x + 150) and (angryIndianman.y > stationary3.y and angryIndianman.y < stationary3.y + 60 or angryIndianman.y + 60 > stationary3.y and angryIndianman.y < stationary3.y):
                        window.blit(stand, (360, 950))
                        game2()
            if (angryIndianman.x + 55 > stationary4.x and angryIndianman.x + 55 < stationary4.x + 150 or angryIndianman.x > stationary4.x and angryIndianman.x < stationary4.x + 150) and (angryIndianman.y > stationary4.y and angryIndianman.y < stationary4.y + 60 or angryIndianman.y + 60 > stationary4.y and angryIndianman.y < stationary4.y):
                        window.blit(stand, (360, 950))
                        game2()                                   
            if angryIndianman.y < 30:
                        angryIndianman.status = 1
                        pygame.display.update()
                        game2()
game1()
pygame.quit()


		
